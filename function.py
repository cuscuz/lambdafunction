import json
import boto3
import urllib3
import os

def lambda_handler(event, context):

	record = event['Records'][0]
	message = record["body"]
	orderId = json.loads(record["messageAttributes"]["order_id"]["stringValue"])
	http = urllib3.PoolManager()
	baseUrl = os.environ['URL_API_ORDER']
	apiKey = os.environ['LAMBDA_API_KEY']

	url = f'{baseUrl}/orders/{orderId}/finalize'
	response = http.request('GET', url, headers={'Authorization': apiKey})

	message = json.loads(record["body"])
	user = message["userName"]
	description = message["productsDescription"]
	totalValue = message["totalValue"]
	emailAddress = message["userEmail"]
	
	body = "Pedido confirmado, " + user + "\n A descrição do pedido é: " + description + "\n Valor total: " + str(totalValue)
    
	ses = boto3.client('ses')
    
	ses.send_email(
		Source = os.environ['SENDER_EMAIL'],
		Destination = {
			'ToAddresses': [
				emailAddress
			]
	    },
		Message = {
			'Subject': {
				'Data': 'Confirmação de pedido',
				'Charset': 'UTF-8'
			},
			'Body': {
				'Text':{
				    'Data': body,
				    'Charset': 'UTF-8'
			    }
		    }
	    }
    )
	return {
        'statusCode': 200,
        'body': json.dumps('Successfully sent email from Lambda using Amazon SQS and SES')
    }
